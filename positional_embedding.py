
import math
from typing import Any, Optional

import torch
from torch import Tensor, nn


class SinusoidalPositionalEmbedding(nn.Module):
  """This module produces sinusoidal positional embeddings of any length.

    Padding symbols are ignored.
    """

  def __init__(self, embedding_dim, padding_idx=0, init_size=500+1):
    super().__init__()
    self.embedding_dim = embedding_dim
    self.padding_idx = padding_idx
    self.weights = self.get_embedding(
        init_size, embedding_dim, padding_idx)
    self.register_buffer("_float_tensor", torch.FloatTensor(1))
    self.max_positions = int(1e5)

  @staticmethod
  def get_embedding(num_embeddings: int,
                    embedding_dim: int,
                    padding_idx: Optional[int] = None):
    """Build sinusoidal embeddings.

        This matches the implementation in tensor2tensor, but differs slightly
        from the description in Section 3.5 of "Attention Is All You Need".
        """
    half_dim = embedding_dim // 2
    emb = math.log(10000) / (half_dim - 1)
    emb = torch.exp(torch.arange(half_dim, dtype=torch.float) * -emb)
    emb = torch.arange(
        num_embeddings, dtype=torch.float).unsqueeze(1) * emb.unsqueeze(0)
    emb = torch.cat([torch.sin(emb), torch.cos(emb)],
                    dim=1).view(num_embeddings, -1)
    if embedding_dim % 2 == 1:
      # zero pad
      emb = torch.cat([emb, torch.zeros(num_embeddings, 1)], dim=1)
    if padding_idx is not None:
      emb[padding_idx, :] = 0
    return emb

  def forward(self, input):
    """Input is expected to be of size [bsz x seqlen]."""
    bsz, seq_len = input.shape
    max_pos = self.padding_idx + 1 + seq_len
    if self.weights is None or max_pos > self.weights.size(0):
      # recompute/expand embeddings if needed
      self.weights = self.get_embedding(
          max_pos, self.embedding_dim, self.padding_idx)
    self.weights = self.weights.to(self._float_tensor)

    positions = make_positions(input, self.padding_idx)
    return (self.weights.index_select(0, positions.view(-1)).view(
        bsz, seq_len, -1).detach())

def make_positions(tensor, padding_idx: int):
  """Replace non-padding symbols with their position numbers.
    Position numbers begin at padding_idx+1. Padding symbols are ignored.
    """
  # The series of casts and type-conversions here are carefully
  # balanced to both work with ONNX export and XLA. In particular XLA
  # prefers ints, cumsum defaults to output longs, and ONNX doesn't know
  # how to handle the dtype kwarg in cumsum.
  mask = tensor.ne(padding_idx).int()
  return (torch.cumsum(mask, dim=1).type_as(mask) * mask).long() + padding_idx

if __name__ == "__main__":
  from modules.position_embedding import SinusoidalPositionalEmbedding as S1
  from positional_embedding import SinusoidalPositionalEmbedding as S2

  head = 9
  dim = head * 5
  dim2 = dim + head
  bs = 7
  s1 = S1(dim)
  s2 = S2(dim)
  for seq_len in range(2,1000):
    q = torch.rand([seq_len, bs, dim])
    p1 = s1(q.transpose(0, 1)[:, :, 0]).transpose(
            0, 1)
    p2 = s2(q.transpose(0, 1)[:, :, 0]).transpose(
            0, 1)
    if not torch.equal(p1,p2):
      print(f'{seq_len}: {torch.equal(p1,p2)}')