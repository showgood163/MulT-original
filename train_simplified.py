import torch
from torch import nn
import torch.nn.functional as F
import torch.optim as optim

import numpy as np
import time



# basic train + result collection
def train(model, optimizer, criterion, train_loader, device, dataset, batch_chunk, gradient_clip, train_chunk):
  loss_total = 0.
  # only countbatch since I drop the last residual batch
  batch_count = 0
  total_batch = len(train_loader) // train_chunk

  model.train()
  for text, audio, vision, eval_attr in train_loader:

    eval_attr = eval_attr.squeeze(-1)  # if num of labels is 1

    text, audio, vision, eval_attr = text.to(device,non_blocking=True), audio.to(device,non_blocking=True), vision.to(device,non_blocking=True), eval_attr.to(device,non_blocking=True)

    model.zero_grad()
    
    if batch_chunk > 1:
      loss=0.
      text_chunks = text.chunk(batch_chunk, dim=0)
      audio_chunks = audio.chunk(batch_chunk, dim=0)
      vision_chunks = vision.chunk(batch_chunk, dim=0)
      eval_attr_chunks = eval_attr.chunk(batch_chunk, dim=0)

      # correct the batch_chunk
      batch_chunk = len(text_chunks)
      for i in range(batch_chunk):
        text_i, audio_i, vision_i = text_chunks[i], audio_chunks[
            i], vision_chunks[i]
        eval_attr_i = eval_attr_chunks[i]
        preds_i, _ = model(text_i, audio_i, vision_i)

        if dataset == 'iemocap':
          preds_i = preds_i.view(-1, 2)
          eval_attr_i = eval_attr_i.view(-1)
        loss_i = criterion(preds_i, eval_attr_i) / batch_chunk
        loss_i.backward()
        loss += loss_i.item()
    else:
      preds, _ = model(text, audio, vision)
      if dataset == 'iemocap':
        preds = preds.view(-1, 2)
        eval_attr = eval_attr.view(-1)
      loss = criterion(preds, eval_attr)
      loss.backward()
      loss = loss.item()

    torch.nn.utils.clip_grad_norm_(model.parameters(), gradient_clip)
    optimizer.step()

    loss_total += loss

    batch_count += 1
    if batch_count == total_batch:
      break
  return loss_total/ total_batch


def test(model, test_loader, device, dataset):
  results = []
  truths = []
  elapsed_time = 0.

  model.eval()
  with torch.no_grad():
    for text, audio, vision, eval_attr in test_loader:
      start_time = time.time()
      eval_attr = eval_attr.squeeze(-1)  # if num of labels is 1
      text, audio, vision, eval_attr = text.to(device,non_blocking=True), audio.to(device,non_blocking=True), vision.to(device,non_blocking=True), eval_attr.to(device,non_blocking=True)
      
      preds, _ = model(text, audio, vision)
      if dataset == 'iemocap':
        preds = preds.view(-1, 2)
        eval_attr = eval_attr.view(-1)
      elapsed_time += time.time() - start_time

      results.append(preds)
      truths.append(eval_attr)

  print(f'Batch infer time: {elapsed_time/len(test_loader)}')
  
  results = torch.cat(results).cpu().detach()
  truths = torch.cat(truths).cpu().detach()

  return results, truths

