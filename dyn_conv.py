import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Function
from math import ceil, floor, gcd, sqrt

from functools import partial
from functools import reduce

import plotly.express as px
import numpy as np


def Linear(in_features, out_features, bias=True):
  m = nn.Linear(in_features, out_features, bias)
  nn.init.xavier_uniform_(m.weight, gain=1 / sqrt(2))
  if bias:
    nn.init.constant_(m.bias, 0.)
  return m


# when n is a square number, the result has 2 same factors
# need to get ordered by abs
def factors(n, K):
  if n >= 0:
    return sorted(reduce(list.__add__, ([(i, n // i)] for i in range(K, 0, -1) if n % i == 0)),key=lambda pair: abs(pair[0]))
  else:
    n = -n
    return sorted(reduce(list.__add__, ([(-i, n // i)] for i in range(K, 0, -1) if n % i == 0)),key=lambda pair: abs(pair[0]))

# working when Ta<=Tb
# Ta*Da->Ta*k->Ta*Tb, directional
# this is able to deal with both aligned and unaligned inputs, just give it 2 sequences.
class DynamicConv(nn.Module):

  def __init__(self,
               kernel_size,
               x_size,
               y_size,
               x_len,
               y_len,
               num_heads=1,
               weight_dropout=0.,
               bias=True,
               alternative=False):

    super(DynamicConv, self).__init__()
    # set the kernel_size for enough overlapping
    ks_base = floor(y_len / x_len) // 2 * 2
    self.kernel_size = kernel_size + ks_base
    
    self.padding = self.kernel_size // 2 if self.kernel_size % 2 == 1 else (
        (self.kernel_size - 1) // 2, self.kernel_size // 2)

    self.num_heads = num_heads
    self.weight_dropout = weight_dropout

    self.x_size = x_size
    self.y_size = y_size
    self.x_len = x_len
    self.y_len = y_len

    self.weight_act = partial(F.softmax, dim=-1)
    self.weight_transform = Linear(
        self.x_size, self.num_heads * self.kernel_size, bias=bias)
    self.output_transform = Linear(self.y_size, self.x_size, bias=bias)

    self.expandParamsCal()

  def forward(self, query, key, value=None, attn_mask=None):
    # query:[Tx,B,Cx]
    # key:[Ty,B,Cy]
    K, H = self.kernel_size, self.num_heads
    Tx, B, Cx = query.size()
    Ty, _, Cy = key.size()

    # Tx,B,Cx -> Tx,B,H*K
    weight = self.weight_transform(query)

    weight = F.dropout(weight, self.weight_dropout, training=self.training)

    # softmax normalize
    # Tx,B,H*K -> Tx*B,H,K
    weight = weight.view(Tx * B, H, K)
    weight = self.weight_act(weight)
    # Tx,B,H*K -> B*H,Tx,K
    weight = weight.view(Tx, B * H, K).transpose(0, 1)

    # expand weight to band matrix
    weight_expanded = self.expandUnaligned(weight, Tx, Ty, B, K, H)

    # bmm and proj
    key = key.view(Ty, B * H, -1).transpose(0, 1)
    output = torch.bmm(weight_expanded, key)
    output = output.transpose(0, 1).contiguous().view(Tx, B, Cy)
    output = self.output_transform(output)

    return output, None  #??? placeholder?

  def expandParamsCal(self):
    Ta = self.x_len
    Tb = self.y_len
    K = self.kernel_size
    H = self.num_heads

    # step_x first priority
    step_x_round = round((Tb - K)/(Ta - 1))
    step_x_lists = sorted(list(range(0,K)),key=lambda key:abs(key-step_x_round))
    self.good_flag = False
    for step_x in step_x_lists:
      # there must be overlap between blocks
      step_y_lists = sorted(list(range(-step_x, K - step_x)), key=lambda key: abs(key))
      for step_y in step_y_lists:
        # at most cut out the length of a kernel
        for out_offset in range(0, K):
          # step_y!=0 means there must be more than 1 block
          if step_y!=0:
            n_blk, res = divmod(Tb + out_offset - K - step_x * (Ta - 1), step_y)
            self.good_good_flag = (res == 0 and n_blk>0)
          else:
            n_blk, res = 0, 0
            self.good_good_flag = (K + step_x * (Ta - 1) == Tb + out_offset)
          if self.good_good_flag:
            # notice that the n_blk above is actually n_blk-1
            # assume that no whole blk is truncated
            n_blk = abs(n_blk)+ 1
            # how to know this solutin actually works?
            # block size of x dir
            blk_szx = ceil(Ta / n_blk)
            # padded total size of x dir
            Tx = blk_szx * n_blk
            # delta size of x dir
            del_x = Tx - Ta

            # padded total size of y dir
            Ty = Tb + step_x * del_x + out_offset
            # padding in x dir
            pad_up = del_x // 2
            pad_down = del_x - pad_up
            # calculate cut position for y dir
            pad_left = pad_up * step_x + out_offset // 2
            if pad_up < blk_szx and pad_down < blk_szx:
              self.good_flag = True

          if self.good_flag:
            break
        if self.good_flag:
          break
      if self.good_flag:
        break
    
    # check the calculation during the init
    if not self.good_flag:
      print(f'Ta={self.x_len},Tb={self.y_len},K={self.kernel_size},no soluion')

    # records
    self.weight_padding_list = (0, 0, pad_up, pad_down)
    self.weight_padded_view_shape = [-1, n_blk, blk_szx, K]
    self.weight_expand_shape = [-1, Tx, Ty]
    self.weight_expand_as_strided_steps = (
        Tx * Ty,
        (Ty + step_x) * blk_szx + step_y,
        Ty + step_x,
        1,
    )
    self.pad_left = pad_left
    self.pad_up = pad_up

    self.step_x = step_x
    self.step_y = step_y
    self.n_blk = n_blk
    self.blk_szx = blk_szx
    self.out_offset = out_offset

  def expandUnaligned(self, weight, Ta, Tb, B, K, H):
    self.weight_expand_shape[0] = self.weight_padded_view_shape[0] = B * H

    weight_padded = F.pad(
        weight,
        self.weight_padding_list,
    ).view(self.weight_padded_view_shape)

    # creat container for the weight
    weight_expanded = weight.new_zeros(
        self.weight_expand_shape, requires_grad=False)
    # copy values using as_strided, notice that when n_blk=1, step_y is not used
    weight_expanded.as_strided(
        self.weight_padded_view_shape,
        self.weight_expand_as_strided_steps).copy_(weight_padded)

    # cut the padded things
    weight_expanded = weight_expanded.narrow(2, self.pad_left, Tb)
    weight_expanded = weight_expanded.narrow(1, self.pad_up, Ta)
    
    # to visualize
    # mat = weight_expanded[1].cpu().detach().squeeze().numpy()
    # fig=px.imshow(mat,title=f'Ta={self.x_len},Tb={self.y_len},K={self.kernel_size},step_x={self.step_x},step_y={self.step_y},blk_szx={self.blk_szx},out_offset={self.out_offset}')
    # fig.show()

    return weight_expanded


def expand3(weight, Ta, Tb, B, K, H):
  # calculate step size of x dir
  real_step = (Tb - K) / (Ta - 1)
  step_x = round(real_step)
  del_step = real_step - step_x
  if K == step_x:
    if del_step == 0.5:
      step_x += 1
    elif del_step == -0.5:
      step_x -= 1
  res = Tb - K - step_x * (Ta - 1)
  # if res >=0, then everything is normal
  if res >= 0:
    # step size == kernel size, then 1 line per block
    if K == step_x:
      n_blk = Ta
    # step size of x dir < 1, then step of y dir = 1
    elif step_x == 0:
      n_blk = Tb - K + 1
    # 0<step size<kernel size
    else:
      n_blk = ceil(res / (K - step_x)) + 1  # upper limit for full coverage
  # or rather, we need to calculate in the reverse dir
  else:
    if del_step == -0.5:  # not specified condition
      n_blk = ceil(-res / step_x) + 1  # why step_x? wrong
    else:
      n_blk = ceil(-res / step_x) + 1

  # block size of x dir
  blk_szx = ceil(Ta / n_blk)
  # padded total size of x dir
  Tx = blk_szx * n_blk
  # delta size of x dir
  del_x = Tx - Ta
  # padded total size of y dir
  Ty = Tb + step_x * del_x
  # step size of y dir need to be calculated
  if step_x != 0:
    # to fit for the last kernel
    if n_blk != 1:
      step_y = (Ty - K - (blk_szx - 1) * step_x) // (n_blk - 1)
    # if only 1 block, then this value will not be used
    else:
      step_y = 0
  # step size of x dir < 1, then step of y dir = 1
  else:
    step_y = 1

  # padding in x dir
  pad_up = del_x // 2
  pad_down = del_x - pad_up
  weight_padded = F.pad(
      weight,
      (0, 0, pad_up, pad_down),
  ).view(B * H, n_blk, blk_szx, K)
  # calculate cut position for y dir
  pad_left = pad_up * step_x

  # creat container for the weight
  weight_expanded = weight.new_zeros(B * H, Tx, Ty, requires_grad=False)
  # copy values using as_strided, notice that when n_blk=1, step_y is not used
  weight_expanded.as_strided((B * H, n_blk, blk_szx, K), (
      Tx * Ty,
      Ty * blk_szx + step_y,
      Ty + step_x,
      1,
  )).copy_(weight_padded)

  # cut the padded things
  weight_expanded = weight_expanded.narrow(2, pad_left, Tb)
  weight_expanded = weight_expanded.narrow(1, pad_up, Ta)
  if weight_expanded[0, 0, :K].nonzero().numel() != K or weight_expanded[
      0, -1, -K:].nonzero().numel() != K:
    print(f'seq_len={Ta},seq_len2={Tb},ks={K}')
    print(weight_expanded[1])

  return weight_expanded


def expand4(weight, Ta, Tb, B, K, H):
  if Ta < Tb:
    for factor in factors(gcd(Ta, Tb)):
      if Tb // factor > K and Ta // factor > 1:
        num_blocks = factor
        break
    Ta, Tb = Ta // num_blocks, Tb // num_blocks

    # here is the priori: the step_size is integer
    step_size = ceil((Tb - K) / (Ta - 1))
    # actual #cols
    Tt = (Ta - 1) * step_size + K

    weight = weight.view(B * H, num_blocks, Ta, K)

    # turn the convolution filters into band matrices, be careful about grads.
    weight_expanded = weight.new_zeros(
        B * H, Ta * num_blocks * num_blocks, Tt, requires_grad=False)
    weight_expanded.as_strided(
        (B * H, num_blocks, Ta, K),
        (Ta * num_blocks * Tt * num_blocks, Ta * Tt * num_blocks + Tt,
         Tt * num_blocks + step_size, 1)).copy_(weight)
    weight_expanded = weight_expanded.narrow(-1, (Tt - Tb) // 2, Tb)
    weight_expanded = weight_expanded.reshape(B * H, Ta * num_blocks,
                                              Tb * num_blocks)
  elif Ta > Tb:
    for factor in factors(gcd(Ta, Tb)):
      if Tb // factor >= K:
        num_blocks = factor
        break
    Ta, Tb = Ta // num_blocks, Tb // num_blocks

    step_size = 1
    patch_num = Tb - K + step_size
    patch_size = ceil(Ta / patch_num)
    Tt = patch_size * patch_num

    pad_up = (Tt - Ta) // 2
    pad_down = Tt - Ta - pad_up
    weight_padded = F.pad(
        weight.view(B * H, num_blocks, Ta, K),
        (0, 0, pad_up, pad_down),
    ).view(B * H, num_blocks, patch_num, patch_size, K)

    weight_expanded = weight.new_zeros(
        B * H, num_blocks, Tt, Tb * num_blocks, requires_grad=False)
    weight_expanded.as_strided((B * H, num_blocks, patch_num, patch_size, K), (
        Tt * num_blocks * Tb * num_blocks,
        Tt * Tb * num_blocks + Tb,
        patch_size * Tb * num_blocks + 1,
        Tb * num_blocks,
        1,
    )).copy_(weight_padded)
    weight_expanded = weight_expanded.narrow(-2, pad_up, Ta).reshape(
        B * H, num_blocks * Ta, num_blocks * Tb)

  else:
    # here is the priori: the step_size is integer
    step_size = ceil((Tb - K) / (Ta - 1))
    # actual #cols
    Tt = (Ta - 1) * step_size + K

    # turn the convolution filters into band matrices, be careful about grads.
    weight_expanded = weight.new_zeros(B * H, Ta, Tt, requires_grad=False)
    weight_expanded.as_strided((B * H, Ta, K),
                               (Ta * Tt, Tt + step_size, 1)).copy_(weight)
    weight_expanded = weight_expanded.narrow(2, (Tt - Tb) // 2, Tb)

  # if weight_expanded[0, 0, 0] == 0 or weight_expanded[0, -1, -1] == 0:
  #   print(weight_expanded[0, 0, :9])
  #   print(weight_expanded[0, 1, :9])
  #   print(weight_expanded[0, -2, -9:])
  #   print(weight_expanded[0, -1, -9:])

  return weight_expanded


def expand2(weight, Ta, Tb, B, K, H):
  step_size = 1
  patch_num = Tb - K + step_size
  patch_size = ceil(Ta / patch_num)
  Tt = patch_size * patch_num

  weight_padded = F.pad(
      weight,
      (0, 0, 0, Tt - Ta),
  ).reshape(B * H, patch_num, patch_size, K)

  weight_expanded = weight.new_zeros(B * H, Tt, Tb, requires_grad=False)
  weight_expanded.as_strided(
      (B * H, patch_num, patch_size, K),
      (Tt * Tb, patch_size * Tb + 1, Tb, 1)).copy_(weight_padded)
  weight_expanded = weight_expanded.narrow(1, 0, Ta)

  return weight_expanded


if __name__ == '__main__':
  head = 9
  dim = head * 5
  dim2 = dim + head
  bs = 7
  for i in range(25,100+1):
    for j in range(16,100+1):
      for sk in range(3, j-floor(j / i) // 2 * 2+1, 2):
        ks = sk
        seq_len = i
        seq_len2 = j

        device = 'cuda' if torch.cuda.is_available() else 'cpu'

        q = torch.rand([seq_len, bs, dim]).to(device)
        k = torch.rand([seq_len2, bs, dim2]).to(device)

        c = DynamicConv(
            ks,
            dim,
            dim2,
            seq_len,
            seq_len2,
            num_heads=head,
            weight_dropout=0.,
        ).to(device)
        c(q, k)
        # input("Press Enter to continue...")