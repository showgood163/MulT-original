import torch
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
import numpy as np
from ax.service.ax_client import AxClient
import ray
from ray import tune
from ray.tune import track
from ray.tune.schedulers import AsyncHyperBandScheduler
from ray.tune.utils import pin_in_object_store, get_pinned_object
from os.path import isfile
import sys

from src.utils import get_data
from src import train
# ray component modified for ax
from ax_search import AxSearch
from repeater_ax import Repeater
from hyparams import hyparams

where = 'lap'
if where == 'lap':
  data_path = '/home/asteria/multimodal-transformer/data'
  gpuId = 0
  num_samples = 2
  resources_per_trial = {"gpu": 0.5}
elif where == 'lab':
  data_path = '/data1/wenhuanglu/multimodal-transformer/data'
  gpuId = 0
  num_samples = 8
  resources_per_trial = {"gpu": 1}

args = hyparams(data_path)

dataset = str.lower(args.dataset.strip())
valid_partial_mode = args.lonly + args.vonly + args.aonly

if valid_partial_mode == 0:
  args.lonly = args.vonly = args.aonly = True
elif valid_partial_mode != 1:
  raise ValueError("You can only choose one of {l/v/a}only.")

use_cuda = False

output_dim_dict = {'mosi': 1, 'mosei_senti': 1, 'iemocap': 8}

criterion_dict = {'iemocap': 'CrossEntropyLoss'}

torch.set_default_tensor_type('torch.FloatTensor')
if torch.cuda.is_available():
  if args.no_cuda:
    print(
        "WARNING: You have a CUDA device, so you should probably not run with --no_cuda"
    )
  else:
    torch.backends.cudnn.benchmark = True
    use_cuda = True

####################################################################
#
# Load the dataset (aligned or non-aligned)
#
####################################################################

print("Start loading the data....")

train_data = get_data(args, dataset, 'train')
valid_data = get_data(args, dataset, 'valid')
test_data = get_data(args, dataset, 'test')

train_loader = DataLoader(train_data, batch_size=args.batch_size, shuffle=True,drop_last=True)
valid_loader = DataLoader(valid_data, batch_size=args.batch_size, shuffle=False)
test_loader = DataLoader(test_data, batch_size=args.batch_size, shuffle=False)

print('Finish loading the data....')
if not args.aligned:
  print("### Note: You are running in unaligned mode.")

####################################################################
#
# Hyperparameters
#
####################################################################

hyp_params = args
hyp_params.orig_d_l, hyp_params.orig_d_a, hyp_params.orig_d_v = train_data.get_dim(
)
hyp_params.l_len, hyp_params.a_len, hyp_params.v_len = train_data.get_seq_len()
hyp_params.use_cuda = use_cuda
hyp_params.dataset = dataset
hyp_params.when = args.when
hyp_params.batch_chunk = args.batch_chunk
hyp_params.n_train, hyp_params.n_valid, hyp_params.n_test = len(
    train_data), len(valid_data), len(test_data)
hyp_params.model = str.upper(args.model.strip())
hyp_params.output_dim = output_dim_dict.get(dataset, 1)
hyp_params.criterion = criterion_dict.get(dataset, 'L1Loss')

# pin the large object in ray
# ray.init()
# tr_loader = pin_in_object_store(train_loader)
# va_loader = pin_in_object_store(valid_loader)
# te_loader = pin_in_object_store(test_loader)


def train_evaluate(parameters):

  # pin and get objects
  train_loader = get_pinned_object(tr_loader)
  valid_loader = get_pinned_object(va_loader)
  test_loader = get_pinned_object(te_loader)

  with torch.cuda.device(gpuId):
    torch.cuda.empty_cache()
    eval_result_list, test_result_list = train.initiate(
        hyp_params,
        train_loader,
        valid_loader,
        test_loader,
    )

  # only tracks test data
  track.log(
      **test_result_list,
      acc_f1=test_result_list['acc_Overall'] + test_result_list['f1_Overall'])


if __name__ == '__main__':
  log_dir = 'records/' + hyp_params.dataset + '_' + (
      'aligned' if hyp_params.aligned else
      'unaligned') + '_' + hyp_params.name + '_' + hyp_params.fusion_type

  mode = 'train'
  if mode == 'eval':
    test_truths=test_data.labels.squeeze()
    test_meta=test_data.meta
    torch.cuda.empty_cache()
    model = torch.load('/home/asteria/dc.pt',map_location='cuda:0')
    with torch.cuda.device(gpuId):
      test_results,_ = train.eval(
      model, hyp_params, valid_loader, test_loader, test=True)
    dc_results = test_results.detach().cpu().squeeze()

    torch.cuda.empty_cache()
    model = torch.load('/home/asteria/ta.pt', map_location='cuda:0')
    with torch.cuda.device(gpuId):
      test_results, _= train.eval(
      model, hyp_params, valid_loader, test_loader, test=True)
    ta_results = test_results.detach().cpu().squeeze()

    delta_dc = test_truths - dc_results
    delta_ta = test_truths - ta_results
    delta_dt = dc_results - ta_results

    k=24
    val, ind = torch.topk(torch.abs(delta_dt), k, 0, True)
    print(dc_results[ind])
    print(ta_results[ind])
    print(test_truths[ind])
    print(test_meta[ind])
    print(delta_dc[ind])
    print(delta_ta[ind])

  elif mode=='train':
    while (True):
      if hyp_params.fusion_type in ['dc','cc','mc']:
        for i in range(1,10):
          hyp_params.kernel_size_av = i
          hyp_params.kernel_size_al = i
          hyp_params.kernel_size_va = i
          hyp_params.kernel_size_vl = i
          hyp_params.kernel_size_la = i
          hyp_params.kernel_size_lv = i
          hyparam_dict = {
                  'kernel_size': i
              }
          # hyparam_dict = {}
          torch.cuda.empty_cache()
          with torch.cuda.device(gpuId):
              eval_result_list, test_result_list = train.initiate(
                  hyp_params, train_loader, valid_loader, test_loader)

              writer = SummaryWriter(log_dir=log_dir)
              writer.add_hparams(
                  hparam_dict=hyparam_dict, metric_dict=eval_result_list)
              writer.add_hparams(
                  hparam_dict=hyparam_dict, metric_dict=test_result_list)
              writer.close()
      elif hyp_params.fusion_type == 'ta':
        for attn_dropout_a in [0., 0.2, 0.25, 0.3]:
          for attn_dropout_v in [0., 0.2, 0.25, 0.3]:
            for out_dropout in [0., 0.05, 0.1, 0.15]:
              hyp_params.attn_dropout_a = attn_dropout_a
              hyp_params.attn_dropout_v = attn_dropout_v
              hyp_params.relu_dropout = out_dropout
              # record
              hyparam_dict = {
                  'attn_dropout_a': attn_dropout_a,
                  'attn_dropout_v': attn_dropout_v,
                  'out_dropout': out_dropout,
              }

              # hyparam_dict = {}
              torch.cuda.empty_cache()
              with torch.cuda.device(gpuId):
                  eval_result_list, test_result_list = train.initiate(
                      hyp_params, train_loader, valid_loader, test_loader)

                  writer = SummaryWriter(log_dir=log_dir)
                  writer.add_hparams(
                      hparam_dict=hyparam_dict, metric_dict=eval_result_list)
                  writer.add_hparams(
                      hparam_dict=hyparam_dict, metric_dict=test_result_list)
                  writer.close()
                  