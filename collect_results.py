from ax.service.ax_client import AxClient
from ax_hyparams import ax_hyparams
from types import SimpleNamespace

import tensorflow as tf
from tensorflow.core.util import event_pb2
from tensorboard.plugins.hparams import plugin_data_pb2
from glob import glob
from os.path import isfile
from pathlib import Path

def get_hparams_and_metrics_from_tb(summary_dir):
  records = []
  f = glob(summary_dir + '/**', recursive=True)
  for filename in f:
    path = Path(filename)
    if isfile(path) and path.suffix not in ['.json','.pkl','.csv']:
      serialized_examples = tf.data.TFRecordDataset(filename)
      hparams = {}
      metrics = {}
      for serialized_example in serialized_examples:
        event = event_pb2.Event.FromString(serialized_example.numpy())
        for value in event.summary.value:
          f = plugin_data_pb2.HParamsPluginData.FromString(
              value.metadata.plugin_data.content)
          if value.tag.endswith('session_start_info'):
            for key, value in f.session_start_info.hparams.items():
              hparams.update({key: value.number_value})
          elif not value.tag.startswith('_hparams_'):
            metrics.update({value.tag: value.simple_value})
      if metrics != {}:
        hparams.update(metrics)
        records.append(hparams)
  return records

def get_arms_and_results_from_ax_client(ax_client):
  arms = ax_client.generation_strategy.trials_as_df
  results = ax_client.experiment.fetch_data().df
  return arms, results

def inject_exps_into_ax_client(arms, results, ax_client):
  arms = arms[arms['Trial Status'] == 'COMPLETED']
  for i in range(arms.shape[0]):
    try:
      params = arms['Arm Parameterizations'][i]
      for param in params.values():
        if param['lr'] == 0.0:
          param['lr']=2e-3
      param, index = ax_client.attach_trial(param)
      ax_client.complete_trial(index, raw_data=(results['mean'][i]/2., results['sem'][i]/4.))
    except KeyError:
      continue
  return ax_client

if __name__ == "__main__":
  hyp_params = {'l_len': 20, 'a_len': 400, 'v_len': 500}
  hyp_params = SimpleNamespace(**hyp_params)

  integrated = AxClient(enforce_sequential_optimization=False)
  integrated.create_experiment(
        name='new_start',
        # Sets max parallelism to 8 for all steps of the generation strategy.
        choose_generation_strategy_kwargs={
            "max_parallelism_override": 2
        },
        objective_name='accf1',
        minimize=False,
        parameters=ax_hyparams(hyp_params)
    )
  
  # fetch data from ax_client
  file_list = ['ax/iemocap_unaligned_dc_search_dc_ax.json', 'ax/iemocap_unaligned_dc_search_dc_random.json']
  for file_name in file_list:
    ax_client = AxClient().load_from_json_file(file_name)
    arms, results = get_arms_and_results_from_ax_client(ax_client)
    integrated = inject_exps_into_ax_client(arms, results, integrated)
  
  # tb_folder_list = ['ax/iemocap_unaligned_dc_search_dc_ax', 'ax/iemocap_unaligned_dc_search_dc_random']
  # for folder_name in tb_folder_list:
  #   records = get_hparams_and_metrics_from_tb(folder_name)

  
  arms, results = get_arms_and_results_from_ax_client(integrated)
  integrated.save_to_json_file('ax/iemocap_unaligned_dc_search_dc_inte.json')
  
  print(1)