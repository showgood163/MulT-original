from math import floor
from types import SimpleNamespace

def ax_hyparams(hyp_params):
  len_dict = {'l': hyp_params.l_len, 'a': hyp_params.a_len, 'v': hyp_params.v_len}
  bound_dict = {}
  for name_i,len_i in len_dict.items():
    for name_j, len_j in len_dict.items():
      # generate upper limit, consider the order first.
      ks_base = floor(len_j / len_i) // 2 * 2
      upper_bound = min(len_j-1,49)
      search_bound = (upper_bound - ks_base) // 2
      bound_dict[name_i+name_j]=search_bound
      # print(f'{len_i},{len_j},{ks_base},{upper_bound},{search_bound}')
  
  ax_hyparams_list = [
    #   {
    #       "name": "kernel_size_av",
    #       "type": "range",
    #       "bounds": [1, bound_dict['av']],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_al",
    #       "type": "range",
    #       "bounds": [1, bound_dict['al']],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_la",
    #       "type": "range",
    #       "bounds": [1, bound_dict['la']],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_lv",
    #       "type": "range",
    #       "bounds": [1, bound_dict['lv']],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_va",
    #       "type": "range",
    #       "bounds": [1, bound_dict['va']],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_vl",
    #       "type": "range",
    #       "bounds": [1, bound_dict['vl']],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
      {
          "name": "kernel_size",
          "type": "range",
          "bounds": [1, 24],
          "value_type": "int",
          "log_scale": False,
      },
    #   {
    #       "name": "kernel_size_vv",
    #       "type": "range",
    #       "bounds": [1, bound_dict['vv']],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_aa",
    #       "type": "range",
    #       "bounds": [1, bound_dict['aa']],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_ll",
    #       "type": "range",
    #       "bounds": [1, bound_dict['ll']],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_v",
    #       "type": "range",
    #       "bounds": [1, 3],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_a",
    #       "type": "range",
    #       "bounds": [1, 3],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "kernel_size_l",
    #       "type": "range",
    #       "bounds": [1, 3],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
      # {
      #     "name": "attn_dropout",
      #     "type": "range",
      #     "bounds": [0.0, 0.3],
      #     "value_type": "float",
      #     "log_scale": False,
      # },
      # {
      #     "name": "attn_dropout_c",
      #     "type": "range",
      #     "bounds": [0.0, 0.3],
      #     "value_type": "float",
      #     "log_scale": False,
      # },
      # {
      #     "name": "attn_dropout_l",
      #     "type": "range",
      #     "bounds": [0.0, 0.3],
      #     "value_type": "float",
      #     "log_scale": False,
      # },
      # {
      #     "name": "attn_dropout_a",
      #     "type": "range",
      #     "bounds": [0.0, 0.3],
      #     "value_type": "float",
      #     "log_scale": False,
      # },
      # {
      #     "name": "attn_dropout_v",
      #     "type": "range",
      #     "bounds": [0.0, 0.3],
      #     "value_type": "float",
      #     "log_scale": False,
      # },
      # {
      #     "name": "embed_dropout",
      #     "type": "range",
      #     "bounds": [0.0, 0.3],
      #     "value_type": "float",
      #     "log_scale": False,
      # },
    #   {
    #       "name": "relu_dropout",
    #       "type": "range",
    #       "bounds": [0, 0.25],
    #       "value_type": "float",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "res_dropout",
    #       "type": "range",
    #       "bounds": [0, 0.25],
    #       "value_type": "float",
    #       "log_scale": False,
    #   },
      # {
      #     "name": "out_dropout",
      #     "type": "range",
      #     "bounds": [0, 0.3],
      #     "value_type": "float",
      #     "log_scale": False,
      # },
      {
          "name": "lr",
          "type": "range",
          "bounds": [5e-4,2e-3],
          "value_type": "float",
          "log_scale": True,
      },
    #   {
    #       "name": "when",
    #       "type": "range",
    #       "bounds": [2, 20],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
    #   {
    #       "name": "es",
    #       "type": "range",
    #       "bounds": [1.,3.],
    #       "value_type": "float",
    #       "log_scale": False,
    #   },
      # {
      #     "name": "beta1",
      #     "type": "range",
      #     "bounds": [1e-4, 0.5],
      #     "value_type": "float",
      #     "log_scale": True,
      # },
      # {
      #     "name": "beta2",
      #     "type": "range",
      #     "bounds": [1e-4, 0.5],
      #     "value_type": "float",
      #     "log_scale": True,
      # },
      # {
      #     "name": "mom",
      #     "type": "range",
      #     "bounds": [0.0001,0.5],
      #     "value_type": "float",
      #     "log_scale": True,
      # },
      # {
      #     "name": "l2",
      #     "type": "range",
      #     "bounds": [0., 0.],
      #     "value_type": "float",
      #     "log_scale": True,
      # },
      # {
      #     "name": "clip",
      #     "type": "range",
      #     "bounds": [0.5, 1.2],
      #     "value_type": "float",
      #     "log_scale": False,
      # },
    #   {
    #       "name": "nlayers",
    #       "type": "range",
    #       "bounds": [3,7],
    #       "value_type": "int",
    #       "log_scale": False,
    #   },
      # {
      #     "name": "num_heads",
      #     "type": "range",
      #     "bounds": [2, 12],
      #     "value_type": "int",
      #     "log_scale": False,
      # },
      # {
      #     "name": "dims_per_head",
      #     "type": "range",
      #     "bounds": [2, 12],
      #     "value_type": "int",
      #     "log_scale": False,
      # },
  ]
  return ax_hyparams_list

if __name__ == "__main__":
  hyp_params = {'l_len': 20, 'a_len': 400, 'v_len': 500}
  ax_hyparams(SimpleNamespace(**hyp_params))