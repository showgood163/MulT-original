from os.path import isfile, join
from os import makedirs
import sys
from tempfile import mkstemp
from types import SimpleNamespace

import torch
from torch.utils.data import DataLoader
from torch import nn
from torch import optim
from radam import RAdam
from torch.optim.lr_scheduler import ReduceLROnPlateau,CosineAnnealingLR

import numpy as np

import ray
from ray import tune
from ray.tune import track
from ray.tune.schedulers import ASHAScheduler, AsyncHyperBandScheduler
from ray.tune.utils import pin_in_object_store, get_pinned_object
from ray.tune.utils import validate_save_restore

# ray component modified for ax
# from ray.tune.suggest.ax import AxSearch
# from ray.tune.suggest import Repeater

from ax_search import AxSearch
from repeater_ax import Repeater
from beam_searcher import BeamSuggestion

from ax.service.ax_client import AxClient

from data_loading import get_data, AVL_Dataset,get_datasets
from train_simplified import train,test
from src.models import MULTModel
from cdcn import CDCNModel
from src.eval_metrics import eval_iemocap, eval_mosei_senti
from hyparams import hyparams
from ax_hyparams import ax_hyparams

where = 'lap'
# according to a paper, optimize the eval acc is okay
early_stop_target = 'mae'
optimize_target = 'mae'
if where == 'lap':
  data_path = '/home/asteria/multimodal-transformer/data'
  num_samples = 2
  resources_per_trial = {"gpu": 1 / num_samples}
  local_mode = False
elif where == 'lab':
  data_path = '/data1/wenhuanglu/multimodal-transformer/data'
  num_samples = 8
  resources_per_trial = {"gpu": 1.}
  local_mode = False


args = hyparams(data_path)

dataset = str.lower(args.dataset.strip())
valid_partial_mode = args.lonly + args.vonly + args.aonly

if valid_partial_mode == 0:
  args.lonly = args.vonly = args.aonly = True
elif valid_partial_mode != 1:
  raise ValueError("You can only choose one of {l/v/a}only.")

output_dim_dict = {'mosi': 1, 'mosei_senti': 1, 'iemocap': 8}

criterion_dict = {'iemocap': 'CrossEntropyLoss'}

torch.backends.cudnn.benchmark = True
use_cuda = True

####################################################################
#
# Load the dataset (aligned or non-aligned)
#
####################################################################

print("Start loading the data....")

train_data, valid_data, test_data = get_datasets(
      args.data_path, dataset, aligned=args.aligned)

train_loader = DataLoader(train_data, batch_size=args.batch_size, shuffle=True,drop_last=True,pin_memory=True)
valid_loader = DataLoader(valid_data, batch_size=args.batch_size, shuffle=False,drop_last=False,pin_memory=True)
test_loader = DataLoader(test_data, batch_size=args.batch_size, shuffle=False,drop_last=False,pin_memory=True)

print('Finish loading the data....')
if not args.aligned:
  print("### Note: You are running in unaligned mode.")

####################################################################
#
# Hyperparameters
#
####################################################################

hyp_params = args
hyp_params.orig_d_l, hyp_params.orig_d_a, hyp_params.orig_d_v = train_data.get_dim(
)
hyp_params.l_len, hyp_params.a_len, hyp_params.v_len = train_data.get_seq_len()
hyp_params.dataset = dataset
hyp_params.batch_chunk = args.batch_chunk
hyp_params.n_train, hyp_params.n_valid, hyp_params.n_test = len(
    train_data), len(valid_data), len(test_data)
hyp_params.model = str.upper(args.model.strip())
hyp_params.output_dim = output_dim_dict.get(dataset, 1)
hyp_params.criterion = criterion_dict.get(dataset, 'L1Loss')

# pin the large object in ray
ray.init(local_mode=local_mode,
    num_cpus=1 if local_mode else None,
    memory=15* 1024 * 1024 * 1024,
    object_store_memory=15*1024 * 1024 * 1024)
tr_loader = pin_in_object_store(train_loader)
va_loader = pin_in_object_store(valid_loader)
te_loader = pin_in_object_store(test_loader)

class ExpWrapper(tune.Trainable):
  def _setup(self, config):
    self.device = torch.device("cuda:0")

    config = SimpleNamespace(**config)

    self.dataset = config.dataset
    self.batch_chunk = config.batch_chunk
    self.gradient_clip = config.clip
    self.train_chunk = config.train_chunk

    # pin and get objects
    if local_mode:
      self.train_loader = train_loader
      self.valid_loader = valid_loader
      self.test_loader = test_loader
    else:
      self.train_loader = get_pinned_object(tr_loader)
      self.valid_loader = get_pinned_object(va_loader)
      self.test_loader = get_pinned_object(te_loader)
    
    if config.model == 'MULT':
      self.model = MULTModel(config).to(self.device)
    elif config.model == 'CDCN':
      self.model = CDCNModel(config).to(self.device)
    
    # self.optimizer = getattr(optim, config.optim)(
    #     self.model.parameters(),
    #     lr = config.lr)
    self.optimizer = optim.Adam(self.model.parameters(),
        lr=config.lr,
        betas=(1-config.beta1, 1-config.beta2),
        weight_decay=config.l2)
    # self.scheduler = ReduceLROnPlateau(self.optimizer, mode='min', patience=config.when, factor=0.1, verbose=True)
    self.scheduler = CosineAnnealingLR(self.optimizer, config.num_epochs)
    self.criterion = getattr(nn, config.criterion)()

    # records the best eval loss
    self.best_eval_loss = 99999999.0
    
  def _train(self):
    # torch.cuda.empty_cache()
    train_loss=train(
        self.model, self.optimizer, self.criterion, self.train_loader, self.device, self.dataset,self.batch_chunk,self.gradient_clip,self.train_chunk)
    results, truths = test(self.model, self.valid_loader, self.device, self.dataset)
    eval_loss = self.criterion(results, truths).item()
    # self.scheduler.step(eval_loss)
    self.scheduler.step()
    # scaling the target is helpful?
    if dataset in ["mosei_senti", 'mosi']:
      eval_results = eval_mosei_senti(results, truths, True)
      eval_accf1 = (eval_results['a7'] + eval_results['f1'] + eval_results['a2'])/3
    elif dataset == 'iemocap':
      eval_results = eval_iemocap(results, truths)
      eval_accf1 = (eval_results['acc_Overall'] + eval_results['f1_Overall'])/2

    if eval_loss < self.best_eval_loss:
      self.best_eval_loss = eval_loss
      results, truths = test(self.model, self.test_loader, self.device, self.dataset)
      if dataset in ["mosei_senti", 'mosi']:
        self.test_results = eval_mosei_senti(results, truths, True)
        test_accf1 = (self.test_results['a7'] + self.test_results['f1'] + self.test_results['a2'])/3
      elif dataset == 'iemocap':
        self.test_results = eval_iemocap(results, truths)
        test_accf1 = (self.test_results['acc_Overall'] + self.test_results['f1_Overall']) / 2
      self.test_results['test_accf1'] = test_accf1

      
    # records
    self.test_results['train_loss'] = train_loss
    self.test_results['eval_accf1'] = eval_accf1
    self.test_results['eval_loss'] = eval_loss
    self.test_results['best_eval_loss'] = self.best_eval_loss

    # wtf, trainable actually not able to early stop since it's not controled by this _train function


    return {**self.test_results}

  def _save(self, tmp_checkpoint_dir):
    checkpoint_path = join(tmp_checkpoint_dir, "model.pth")
    torch.save(self.model.state_dict(), checkpoint_path)
    return tmp_checkpoint_dir

  def _restore(self, tmp_checkpoint_dir):
    checkpoint_path = join(tmp_checkpoint_dir, "model.pth")
    self.model.load_state_dict(torch.load(checkpoint_path))

def ExpFunc(config):
  device = torch.device("cuda:0")

  config = SimpleNamespace(**config)

  dataset = config.dataset
  batch_chunk = config.batch_chunk
  gradient_clip = config.clip
  train_chunk = config.train_chunk

  if not local_mode:
    train_loader = get_pinned_object(tr_loader)
    valid_loader = get_pinned_object(va_loader)
    test_loader = get_pinned_object(te_loader)
  else:
    train_loader = train_loader
    valid_loader = valid_loader
    test_loader = test_loader
  
  model = config.model
  if model == 'MULT':
    model = MULTModel(config).to(device)
  elif model == 'CDCN':
    model = CDCNModel(config).to(device)

  # optimizer = getattr(optim, config.optim)(
  #     model.parameters(),
  #     lr = config.lr)
  optimizer = optim.Adam(model.parameters(),
      lr=config.lr,
      # the betas are not the same when searching and not searching
      betas=(1-config.beta1, 1-config.beta2),
      weight_decay=config.l2)
  # scheduler = ReduceLROnPlateau(optimizer, mode='min', patience=config.when, factor=0.5, verbose=True)
  scheduler = CosineAnnealingLR(optimizer,config.num_epochs)
  criterion = getattr(nn, config.criterion)()

  best_eval_loss = 99999999.0
  early_stop_period = 0
  early_stop_threshold = round(config.es*config.when)
  while True:
    train_loss=train(model, optimizer, criterion, train_loader, device, dataset,batch_chunk,gradient_clip,train_chunk)
    results, truths = test(model, valid_loader, device, dataset)
    eval_loss = criterion(results, truths).item()
    # scheduler.step(eval_loss)
    scheduler.step()
    # scaling the target is helpful?
    if dataset in ["mosei_senti", 'mosi']:
      eval_results = eval_mosei_senti(results, truths, True)
      eval_accf1 = (eval_results['a7'] + eval_results['f1'] + eval_results['a2'])/3
    elif dataset == 'iemocap':
      eval_results = eval_iemocap(results, truths)
      eval_accf1 = (eval_results['acc_Overall'] + eval_results['f1_Overall']) / 2
    
    # choose the model based on eval_loss
    if eval_loss < best_eval_loss:
      best_eval_loss = eval_loss
      results, truths = test(model, test_loader, device, dataset)
      if dataset in ["mosei_senti", 'mosi']:
        test_results = eval_mosei_senti(results, truths, True)
        test_accf1 = (test_results['a7'] + test_results['f1'] + test_results['a2'])/3
      elif dataset == 'iemocap':
        test_results = eval_iemocap(results, truths)
        test_accf1 = (test_results['acc_Overall'] + test_results['f1_Overall']) / 2
      test_results['test_accf1'] = test_accf1

      early_stop_period = 0
    else:
      early_stop_period += 1

    # records
    test_results['train_loss'] = train_loss
    test_results['eval_accf1'] = eval_accf1
    test_results['eval_loss'] = eval_loss
    test_results['best_eval_loss'] = best_eval_loss
    
    track.log(**test_results)
    if early_stop_period > early_stop_threshold:
      break

if __name__ == '__main__':
  beam_search = False
  beam_size = 16 # depend on the perf of these hyparams!
  mode = 'search_ks_lr_cos_es_10'

  log_dir = 'ax/' + hyp_params.dataset + '_' + (
      'aligned' if hyp_params.aligned else
      'unaligned') + '_' + hyp_params.model + '_' + hyp_params.fusion_type + '_' + mode
  ax_file = log_dir + '.json'
  if isfile(ax_file):
    ax_client = AxClient.load_from_json_file(ax_file)
  else:
    ax_client = AxClient(enforce_sequential_optimization=False)
    ax_client.create_experiment(
        name=hyp_params.name,
        # Sets max parallelism to 8 for all steps of the generation strategy.
        # choose_generation_strategy_kwargs={
        #     "max_parallelism_override": num_samples
        # },
        objective_name=optimize_target,
        minimize=not optimize_target.endswith('accf1'),
        parameters=ax_hyparams(hyp_params),
        choose_generation_strategy_kwargs={"no_bayesian_optimization": mode == 'random'} ,
    )
  if beam_search:
    print('beam search mode!')
    def read_pickle_file(filename):
      import pickle
      with open(filename, 'rb') as file:
          return pickle.load(file)
    # load results and select the best
    beam_search_hyparams_list = read_pickle_file(log_dir + '.pkl')[-beam_size:]
    algo = BeamSuggestion(
      max_concurrent=num_samples,
      beam_configs=beam_search_hyparams_list)
  else:
    algo = AxSearch(ax_client, max_concurrent=num_samples, save_path=ax_file)

  re_search_alg = Repeater(algo, repeat=max(2,num_samples)) # may need to be changed manually
  sched = AsyncHyperBandScheduler(
      time_attr='training_iteration',
      metric=early_stop_target,
      mode='max' if early_stop_target.endswith('accf1') else 'min',
      max_t=hyp_params.num_epochs,
      grace_period=8*4 if beam_search else 8,
      reduction_factor=2,
      brackets=1 if beam_search else 2,
  )
  
  # both of these should return
  # validate_save_restore(ExpWrapper)
  # validate_save_restore(ExpWrapper, use_object_store=True)
  
  while (True):
    try:
      tune.run(
          ExpWrapper if local_mode else ExpFunc,
          search_alg=re_search_alg,
          scheduler=sched,
          # long enough to not restart
          num_samples=999,
          # Set this level to 1 to see status updates and to 2 to also see trial results.
          verbose=1,
          resources_per_trial=resources_per_trial,
          # checkpoint_at_end=True,
          config={
              'tr_data' : tr_loader,
              'va_data' : va_loader,
              'te_data' : te_loader,
              **hyp_params.__dict__,
          },
          stop={
            "training_iteration": 1 if local_mode else hyp_params.num_epochs,
          },
          local_dir='/tmp/ray_results' if local_mode else log_dir,
          checkpoint_at_end=local_mode,
          checkpoint_freq=1 if local_mode else 0,
          )
    # error handling, how?
    except:
      # mark cuda OOM as failure and other errors as debugging signal
      err = sys.exc_info()[1]
      print(err)
      if str(err).startswith('CUDA'):
        pass
      else:
        break
