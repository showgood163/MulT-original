import torch
from torch import nn
from torch.nn import LayerNorm
import torch.nn.functional as F
from positional_embedding import SinusoidalPositionalEmbedding
from dyn_conv import DynamicConv
from my_cdc import MyCDC
from math import ceil


class IntermodalInteractionModule(nn.Module):

  def __init__(self,
               embed_dim,
               x_len,
               y_len,
               ks,
               num_heads,
               nlayers,
               attn_dropout=0.0,
               relu_dropout=0.0,
               res_dropout=0.0,
               embed_dropout=0.0):
    super().__init__()
    self.dropout = embed_dropout  # Embedding dropout
    self.attn_dropout = attn_dropout
    self.embed_dim = embed_dim
    self.embed_positions = None # SinusoidalPositionalEmbedding(embed_dim)

    self.layers = nn.ModuleList([])
    for layer in range(nlayers):
      new_layer = IntermodalInteractionLayer(
          embed_dim,
          x_len,
          y_len,
          ks,
          num_heads=num_heads,
          attn_dropout=attn_dropout,
          relu_dropout=relu_dropout,
          res_dropout=res_dropout)
      self.layers.append(new_layer)

    self.register_buffer('version', torch.Tensor([2]))
    self.normalize = True
    if self.normalize:
      self.layer_norm = LayerNorm(embed_dim)

  def forward(self, x, x_k=None):
    # if self.embed_positions is not None:
    #   x += self.embed_positions(x.transpose(0, 1)[:, :, 0]).transpose(
    #       0, 1)  # Add positional embedding
    x = F.dropout(x, p=self.dropout, training=self.training)

    if x_k is not None:
      # if self.embed_positions is not None:
      #   x_k += self.embed_positions(x_k.transpose(0, 1)[:, :, 0]).transpose(
      #       0, 1)  # Add positional embedding
      for layer in self.layers:
        x_kd = F.dropout(x_k, p=self.dropout, training=self.training)
        x = layer(x, x_kd)
    else:
      for layer in self.layers:
        x = layer(x)

    if self.normalize:
      x = self.layer_norm(x)

    return x


# This is able to run as both Inter and Intra-modal interaction layer
class IntermodalInteractionLayer(nn.Module):

  def __init__(self,
               embed_dim,
               x_len,
               y_len,
               ks,
               num_heads=4,
               attn_dropout=0.1,
               relu_dropout=0.1,
               res_dropout=0.1,
               encoder_normalize_before=True):
    super().__init__()
    self.embed_dim = embed_dim
    self.core = MyCDC(
        ks,
        embed_dim,
        embed_dim,
        x_len,
        y_len,
        num_heads=num_heads,
        weight_dropout=attn_dropout)
    self.layer_norm_q = LayerNorm(self.embed_dim)
    self.layer_norm_k = LayerNorm(self.embed_dim)
    self.relu_dropout = relu_dropout
    self.res_dropout = res_dropout
    self.activation_fn = F.relu

    self.normalize_before = encoder_normalize_before
    self.fc = Linear(self.embed_dim, self.embed_dim)
    self.middle_layer_norm_q = LayerNorm(self.embed_dim)

  def forward(self, q, k=None):
    residual = q
    # LN before residual function
    q = self.maybe_layer_norm(self.layer_norm_q, q, before=True)

    # if multimodal input, then LN is needed
    if k is None:
      q, _ = self.core(query=q, key=q)
    else:
      k = self.maybe_layer_norm(self.layer_norm_k, k, before=True)
      q, _ = self.core(query=q, key=k)

    q = self.middle_layer_norm_q(q)
    q = self.activation_fn(q)
    q = F.dropout(q, p=self.relu_dropout, training=self.training)

    q = self.fc(q)
    q = F.dropout(q, p=self.res_dropout, training=self.training)
    q = residual + q
    # LN after residual merging
    q = self.maybe_layer_norm(self.layer_norm_q, q, after=True)

    return q

  def maybe_layer_norm(self, layer_norm, x, before=False, after=False):
    assert before ^ after
    if after ^ self.normalize_before:
      return layer_norm(x)
    else:
      return x


def Linear(in_features, out_features, bias=True):
  m = nn.Linear(in_features, out_features, bias)
  nn.init.xavier_uniform_(m.weight)
  if bias:
    nn.init.constant_(m.bias, 0.)
  return m


if __name__ == '__main__':
  head = 9
  dim = head * 5
  dim2 = dim + head
  bs = 7
  for i in [20, 500, 400]:
    for j in [20, 500, 400]:
      for sk in range(ceil(j / i), ceil(j / i) + 10):
        ks = sk
        seq_len = i
        seq_len2 = j
        # seq_len, seq_len2, ks = 500, 400, 9
        # print(f'seq_len={i},seq_len2={j},ks={ks}')
        # print(f'Ta={seq_len},Tb={seq_len2},ks=ks')

        device = 'cuda' if torch.cuda.is_available() else 'cpu'

        q = torch.rand([seq_len, bs, dim]).to(device)
        k = torch.rand([seq_len2, bs, dim]).to(device)
        c = IntermodalInteractionLayer(
            dim, seq_len, seq_len2, ks, num_heads=head).to(device)
        c(q, k)
        c = IntermodalInteractionLayer(
            dim, seq_len, seq_len, ks - ceil(j / i) + 1,
            num_heads=head).to(device)
        c(q)
        c = IntermodalInteractionModule(dim, seq_len, seq_len2,
                                        ks - ceil(j / i) + 1, head,
                                        3).to(device)
        c(q, k)
        c = IntermodalInteractionModule(dim, seq_len, seq_len,
                                        ks - ceil(j / i) + 1, head,
                                        3).to(device)
        c(q)
        # input("Press Enter to continue...")
