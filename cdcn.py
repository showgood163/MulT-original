import torch
from torch import nn
import torch.nn.functional as F

from intermodal_interaction_layer import IntermodalInteractionModule
from modules.transformer import TransformerEncoder


class CDCNModel(nn.Module):

  def __init__(self, hyp_params):
    super(CDCNModel, self).__init__()
    self.orig_d_l, self.orig_d_a, self.orig_d_v = hyp_params.orig_d_l, hyp_params.orig_d_a, hyp_params.orig_d_v
    dim = hyp_params.dims_per_head * hyp_params.num_heads
    self.d_l, self.d_a, self.d_v = dim, dim, dim
    self.fusion_type = hyp_params.fusion_type
    self.l_len, self.a_len, self.v_len = hyp_params.l_len, hyp_params.a_len, hyp_params.v_len
    # 1d conv
    self.kernel_size_a = hyp_params.kernel_size_a
    self.kernel_size_l = hyp_params.kernel_size_l
    self.kernel_size_v = hyp_params.kernel_size_v
    # shrinked search space for ccc/cdc
    self.kernel_size_av = hyp_params.kernel_size
    self.kernel_size_vl = hyp_params.kernel_size
    self.kernel_size_la = hyp_params.kernel_size
    self.kernel_size_al = hyp_params.kernel_size
    self.kernel_size_va = hyp_params.kernel_size
    self.kernel_size_lv = hyp_params.kernel_size
    # not used since I use attention instead.
    self.kernel_size_aa = hyp_params.kernel_size_aa
    self.kernel_size_ll = hyp_params.kernel_size_ll
    self.kernel_size_vv = hyp_params.kernel_size_vv
    self.vonly = hyp_params.vonly
    self.aonly = hyp_params.aonly
    self.lonly = hyp_params.lonly
    self.num_heads = hyp_params.num_heads
    self.nlayers = hyp_params.nlayers
    self.attn_dropout = hyp_params.attn_dropout
    self.attn_dropout_l = hyp_params.attn_dropout_l
    self.attn_dropout_a = hyp_params.attn_dropout_a
    self.attn_dropout_v = hyp_params.attn_dropout_v
    self.relu_dropout = hyp_params.relu_dropout
    self.res_dropout = hyp_params.res_dropout
    self.out_dropout = hyp_params.out_dropout
    self.embed_dropout = hyp_params.embed_dropout
    self.attn_mask = hyp_params.attn_mask

    combined_dim = self.d_l + self.d_a + self.d_v

    self.partial_mode = self.lonly + self.aonly + self.vonly
    if self.partial_mode == 1:
      if self.lonly:
        combined_dim = 2 * self.d_l  
      elif self.aonly:
        combined_dim = 2 * self.d_a 
      elif self.vonly:
        combined_dim = 2 * self.d_v
    else:
      combined_dim = 2 * (self.d_l + self.d_a + self.d_v)

    output_dim = hyp_params.output_dim  # This is actually not a hyperparameter :-)

    # 1. Temporal convolutional layers
    self.proj_l = nn.Conv1d(
        self.orig_d_l,
        self.d_l,
        kernel_size=self.kernel_size_l * 2 + 1,
        padding=self.kernel_size_l,
        bias=False)
    self.proj_a = nn.Conv1d(
        self.orig_d_a,
        self.d_a,
        kernel_size=self.kernel_size_a * 2 + 1,
        padding=self.kernel_size_a,
        bias=False)
    self.proj_v = nn.Conv1d(
        self.orig_d_v,
        self.d_v,
        kernel_size=self.kernel_size_v * 2 + 1,
        padding=self.kernel_size_v,
        bias=False)

    # 2. Crossmodal Attentions
    if self.lonly:
      self.trans_l_with_a = self.get_network(self_type='la')
      self.trans_l_with_v = self.get_network(self_type='lv')
    if self.aonly:
      self.trans_a_with_l = self.get_network(self_type='al')
      self.trans_a_with_v = self.get_network(self_type='av')
    if self.vonly:
      self.trans_v_with_l = self.get_network(self_type='vl')
      self.trans_v_with_a = self.get_network(self_type='va')

    # 3. Self Attentions (Could be replaced by LSTMs, GRUs, etc.)
    #    [e.g., self.trans_x_mem = nn.LSTM(self.d_x, self.d_x, 1)
    if self.lonly:
      self.trans_l_mem = self.get_network(self_type='l_mem', layers=3)
    if self.aonly:
      self.trans_a_mem = self.get_network(self_type='a_mem', layers=3)
    if self.vonly:
      self.trans_v_mem = self.get_network(self_type='v_mem', layers=3)

    # Projection layers
    self.proj1 = nn.Linear(combined_dim, combined_dim)
    self.proj2 = nn.Linear(combined_dim, combined_dim)
    self.out_layer = nn.Linear(combined_dim, output_dim)

  def get_network(self, self_type='l', layers=-1):
    if self_type in ['l', 'al', 'vl']:
      embed_dim, attn_dropout = self.d_l, self.attn_dropout_l
    elif self_type in ['a', 'la', 'va']:
      embed_dim, attn_dropout = self.d_a, self.attn_dropout_a
    elif self_type in ['v', 'lv', 'av']:
      embed_dim, attn_dropout = self.d_v, self.attn_dropout_v
    elif self_type == 'l_mem':
      embed_dim, attn_dropout = 2 * self.d_l, self.attn_dropout
    elif self_type == 'a_mem':
      embed_dim, attn_dropout = 2 * self.d_a, self.attn_dropout
    elif self_type == 'v_mem':
      embed_dim, attn_dropout = 2 * self.d_v, self.attn_dropout
    else:
      raise ValueError("Unknown network type")

    if len(self_type) == 2:
      if self_type[0] == 'a':
        x_len = self.a_len
      elif self_type[0] == 'v':
        x_len = self.v_len
      elif self_type[0] == 'l':
        x_len = self.l_len

      if self_type[1] == 'a':
        y_len = self.a_len
      elif self_type[1] == 'v':
        y_len = self.v_len
      elif self_type[1] == 'l':
        y_len = self.l_len
      fusion_type = self.fusion_type

      if self_type == 'av':
        kernel_size = self.kernel_size_av
      elif self_type == 'al':
        kernel_size = self.kernel_size_al
      elif self_type == 'la':
        kernel_size = self.kernel_size_la
      elif self_type == 'lv':
        kernel_size = self.kernel_size_lv
      elif self_type == 'vl':
        kernel_size = self.kernel_size_vl
      elif self_type == 'va':
        kernel_size = self.kernel_size_va
    else:
      if self_type[0] == 'a':
        x_len, y_len = self.a_len, self.a_len
        kernel_size = self.kernel_size_aa
      elif self_type[0] == 'v':
        x_len, y_len = self.v_len, self.v_len
        kernel_size = self.kernel_size_vv
      elif self_type[0] == 'l':
        x_len, y_len = self.l_len, self.l_len
        kernel_size = self.kernel_size_ll
    
    if len(self_type) == 2:
      return IntermodalInteractionModule(
          embed_dim=embed_dim,
          x_len=x_len,
          y_len=y_len,
          ks=kernel_size * 2 + 1,
          num_heads=self.num_heads,
          nlayers=self.nlayers,
          attn_dropout=attn_dropout,
          relu_dropout=self.relu_dropout,
          res_dropout=self.res_dropout,
          embed_dropout=self.embed_dropout)
    else:
      # For using conv on the self att part, I didn't find a good solution using conv under the conditon that #layers is very low.
      # I changed the output position to the middle and the last-kernel_size//2 but that leads to a very large kernel size instead.
      # change back to the ta.
      return TransformerEncoder(
        fusion_type='ta',
        embed_dim=embed_dim,
        x_len=x_len,
        y_len=y_len,
        ks=kernel_size * 2 + 1,
        num_heads=self.num_heads,
        nlayers=max(self.nlayers, layers),
        attn_dropout=attn_dropout,
        relu_dropout=self.relu_dropout,
        res_dropout=self.res_dropout,
        embed_dropout=self.embed_dropout,
        attn_mask=self.attn_mask)

  def forward(self, x_l, x_a, x_v):
    """
        text, audio, and vision should have dimension [batch_size, seq_len, n_features]
        """
    x_l = F.dropout(
        x_l.transpose(1, 2), p=self.embed_dropout, training=self.training)
    x_a = x_a.transpose(1, 2)
    x_v = x_v.transpose(1, 2)

    # Project the textual/visual/audio features
    proj_x_l = x_l if self.orig_d_l == self.d_l else self.proj_l(x_l)
    proj_x_a = x_a if self.orig_d_a == self.d_a else self.proj_a(x_a)
    proj_x_v = x_v if self.orig_d_v == self.d_v else self.proj_v(x_v)
    proj_x_a = proj_x_a.permute(2, 0, 1)
    proj_x_v = proj_x_v.permute(2, 0, 1)
    proj_x_l = proj_x_l.permute(2, 0, 1)

    if self.lonly:
      # (V,A) --> L
      h_l_with_as = self.trans_l_with_a(proj_x_l,
                                        proj_x_a)  # Dimension (L, N, d_l)
      h_l_with_vs = self.trans_l_with_v(proj_x_l,
                                        proj_x_v)  # Dimension (L, N, d_l)
      h_ls = torch.cat([h_l_with_as, h_l_with_vs], dim=2)
      h_ls = self.trans_l_mem(h_ls)
      if type(h_ls) == tuple:
        h_ls = h_ls[0]
      last_h_l = last_hs = h_ls[-1]  # Take the last output for prediction

    if self.aonly:
      # (L,V) --> A
      h_a_with_ls = self.trans_a_with_l(proj_x_a, proj_x_l)
      h_a_with_vs = self.trans_a_with_v(proj_x_a, proj_x_v)
      h_as = torch.cat([h_a_with_ls, h_a_with_vs], dim=2)
      h_as = self.trans_a_mem(h_as)
      if type(h_as) == tuple:
        h_as = h_as[0]
      last_h_a = last_hs = h_as[-1]

    if self.vonly:
      # (L,A) --> V
      h_v_with_ls = self.trans_v_with_l(proj_x_v, proj_x_l)
      h_v_with_as = self.trans_v_with_a(proj_x_v, proj_x_a)
      h_vs = torch.cat([h_v_with_ls, h_v_with_as], dim=2)
      h_vs = self.trans_v_mem(h_vs)
      if type(h_vs) == tuple:
        h_vs = h_vs[0]
      last_h_v = last_hs = h_vs[-1]

    if self.partial_mode == 3:
      last_hs = torch.cat([last_h_l, last_h_a, last_h_v], dim=1)

    # A residual block
    last_hs_proj = self.proj2(
        F.dropout(
            F.relu(self.proj1(last_hs)),
            p=self.out_dropout,
            training=self.training))
    last_hs_proj += last_hs

    output = self.out_layer(last_hs_proj)
    return output, last_hs
